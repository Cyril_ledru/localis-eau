-- comptage du nombre de prélèvement sur UDI_PLV_2021 pour le Grand Est
select count(*) 
from UDI_PLV_2021 where cddept ='008' or cddept='010'  or cddept='051' or cddept='052' or cddept='054' or cddept='055' or cddept='057'  or cddept='067' or cddept='068' or cddept='088'

-- comptage du nombre de prélèvement sur UDI_PL_2021 pour le Grand Est avec des coordonnées X

select count(*) 
from UDI_PLV_2021 
where coord_x is NOT NULL and (cddept ='008' or cddept='010'  or cddept='051' or cddept='052' or cddept='054' or cddept='055' or cddept='057'  or cddept='067' or cddept='068' or cddept='088')

-- Jointure entre les 2 tables PLV_RES afin d'avoir les données sur le nitrate NO3
select * 
from UDI_PLV_2021,UDI_RES_2021
where coord_x is NOT NULL and (UDI_PLV_2021.cddept ='008' or UDI_PLV_2021.cddept='010'  or UDI_PLV_2021.cddept='051' or UDI_PLV_2021.cddept='052' 
or UDI_PLV_2021.cddept='054' or UDI_PLV_2021.cddept='055' or UDI_PLV_2021.cddept='057'  or UDI_PLV_2021.cddept='067' or UDI_PLV_2021.cddept='068' 
or UDI_PLV_2021.cddept='088') 
and UDI_PLV_2021.referenceprel=UDI_RES_2021.referenceprel and cdparametresiseeaux='NO3'